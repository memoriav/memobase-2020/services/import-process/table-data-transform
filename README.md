## Table Data Transform Service

[Confluence Doku](https://memobase.atlassian.net/wiki/spaces/TBAS/pages/48693312/Service+Table-Data+Transform)

### What does it do?

This service reads files with table data serialized as CSV, TSV, XLS or XLSX and transforms them into a flat
JSON structure. 

The service has a few sanity checks to ensure that all the properties have a valid name and all the defined identifiers
are unique across all contained records.