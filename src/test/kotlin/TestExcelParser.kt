/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.schema.Message
import ch.memobase.settings.HeaderMetadata
import ch.memobase.utility.AcceptedFileFormat
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.io.FileInputStream
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestExcelParser {

    private val parser = TableParser(Reports("test", "testVersion"))
    private val header = HeaderMetadata(
        "",
        "",
        "",
        true,
        "",
        "",
        1,
        1,
        1,
        1
    )

    private fun reader(filename: String): String {
        return FileInputStream(File("src/test/resources/excel/$filename.json")).bufferedReader().lines()
            .reduce { t, t2 -> t + t2 }
            .orElse("")
    }

    @Test
    fun `test excel file with formulas`() {
        val result = parser.parseTable(
            "key",
            Message(AcceptedFileFormat.XLSX, ""),
            header,
            FileInputStream(File("src/test/resources/excel/test1.xlsx"))
        )
        val output = result[0].value.let {
            if (it != null) {
                Json.encodeToString(it)
            } else {
                ""
            }
        }

        assertThat(output)
            .isEqualTo(reader("test1.output"))
    }
}