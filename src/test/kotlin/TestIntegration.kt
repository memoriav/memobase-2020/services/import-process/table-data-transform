/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.Report
import ch.memobase.testing.EmbeddedSftpServer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromStream
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset
import java.nio.file.Paths
import java.util.stream.Stream.of

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestIntegration {

    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    private val sftpServer = EmbeddedSftpServer(22000, "user", "password")

    init {
        val files = listOf(
            Pair("/memobase/test-2", "test-2.csv"),
            Pair("/memobase/test-3", "test-3.csv"),
            Pair("/memobase/test-4", "test-4.xlsx"),
            Pair("/memobase/test-5", "test-5.xlsx")
        )

        for (pair in files) {
            sftpServer.putFile(
                Paths.get(pair.first, pair.second).toString(),
                FileInputStream(Paths.get("src/test/resources/sftp", pair.second).toFile())
            )
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    @ParameterizedTest
    @MethodSource("testParams")
    fun `test kafka integrations`(params: TestParams) {
        val service = Service(params.settingsFileName)
        val testDriver = TopologyTestDriver(service.topology, service.settings.kafkaStreamsSettings)

        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", params.recordSetId.toByteArray()))
        headers.add(RecordHeader("institutionId", "test-institution-id".toByteArray()))
        headers.add(RecordHeader("isPublished", "false".toByteArray()))
        headers.add(RecordHeader("xmlRecordTag", "record".toByteArray()))
        headers.add(RecordHeader("xmlIdentifierFieldName", "identifierMain".toByteArray()))
        headers.add(RecordHeader("tableSheetIndex", params.tableSheetIndex.toString().toByteArray()))
        headers.add(RecordHeader("tableHeaderCount", params.tableHeaderCount.toString().toByteArray()))
        headers.add(RecordHeader("tableHeaderIndex", params.tableHeaderIndex.toString().toByteArray()))
        headers.add(RecordHeader("tableIdentifierIndex", params.tableIdentifierIndex.toString().toByteArray()))

        testDriver.use { driver ->
            val inputTopic =
                driver.createInputTopic(service.settings.inputTopic, StringSerializer(), StringSerializer())
            val outputTopic =
                driver.createOutputTopic(service.settings.outputTopic, StringDeserializer(), StringDeserializer())
            val reportTopic = driver.createOutputTopic(
                service.settings.processReportTopic, StringDeserializer(), StringDeserializer()
            )

            inputTopic.pipeInput(
                TestRecord(
                    params.inputKey, readFile("${params.count}/input-message.json"), headers
                )
            )

            val records = outputTopic.readRecordsToList()
            val keys = records.map { it.key }
            val values = records.map { Json.decodeFromString<JsonObject>(it.value) }
            val reports = reportTopic.readRecordsToList().map { Json.decodeFromString<Report>(it.value) }
            val expectedOutputs = mutableListOf<JsonObject>()
            val expectedReports = mutableListOf<Report>()

            File("$resourcePath/${params.count}").walk(FileWalkDirection.TOP_DOWN)
                .forEach { file ->
                    if (file.isFile) {
                        if (file.name.startsWith("output-message")) {
                            expectedOutputs.add(Json.decodeFromStream(FileInputStream(file)))
                        } else if (file.name.startsWith("output-report")) {
                            expectedReports.add(Json.decodeFromStream(FileInputStream(file)))
                        }
                    }

                }
            assertAll(
                "",
                {
                    assertThat(keys)
                        .hasSize(params.expectedOutputKeys.size)
                },
                {
                    assertThat(keys)
                        .containsAll(params.expectedOutputKeys)
                },
                {
                    assertThat(values)
                        .hasSize(expectedOutputs.size)
                },
                {
                    assertThat(values)
                        .containsAll(expectedOutputs)
                },
                {
                    assertThat(reports)
                        .hasSize(expectedReports.size)
                },
                {
                    assertThat(reports)
                        .containsAll(expectedReports)
                }
            )
        }






    }

    private fun testParams() = of(
        TestParams(
            "Test XML Filter",
            1,
            "test-1.yml",
            "test-1.xml",
            emptyList(),
            "test-1",
            0, 3, 3, 1
        ),
        TestParams(
            "Test Valid CSV Input",
            2,
            "test-2.yml",
            "test-2.csv",
            listOf("test-id-1", "test-id-2"),
            "test-2",
            0, 3, 3, 1
        ),
        TestParams(
            "Test Invalid CSV Input",
            3,
            "test-3.yml",
            "test-3.csv",
            listOf(),
            "test-3",
            0, 3, 3, 1
        ),
        TestParams(
            "Test Valid XSLX Input",
            4,
            "test-4.yml",
            "test-4.xlsx",
            listOf("Test-1", "Test-2"),
            "test-4",
            1, 1, 1, 1
        ),
        TestParams(
            "Test Duplicate Keys",
            5,
            "test-5.yml",
            "test-5.xlsx",
            listOf("Test-1"),
            "test-5",
            1, 1, 1, 1
        )
    )
}
