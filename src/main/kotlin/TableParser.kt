/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.UtilityFunctions.retrieveCellValue
import ch.memobase.UtilityFunctions.retrieveCells
import ch.memobase.exceptions.InvalidInputException
import ch.memobase.schema.Message
import ch.memobase.schema.ReportMessages
import ch.memobase.schema.ResultMessage
import ch.memobase.settings.HeaderMetadata
import ch.memobase.utility.AcceptedFileFormat
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.github.doyaaaaaken.kotlincsv.util.CSVFieldNumDifferentException
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import org.apache.logging.log4j.LogManager
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.InputStream

class TableParser(private val reports: Reports) {
    private val log = LogManager.getLogger(this::class.java)
    private val invalidPropertyNameCharacters = listOf('.', ':', '/', '+')

    fun parseTable(
        key: String,
        inputMessage: Message,
        metadata: HeaderMetadata,
        inputStream: InputStream
    ): List<ResultMessage> {
        return try {
            when (inputMessage.format) {
                AcceptedFileFormat.XLS, AcceptedFileFormat.XLSX -> excelMapper(key, metadata, inputStream)
                AcceptedFileFormat.CSV, AcceptedFileFormat.TSV -> csvMapper(key, inputMessage, metadata, inputStream)
                else -> throw InvalidInputException("Cannot parse the table with format ${inputMessage.format}.")
            }
        } catch (ex: CSVFieldNumDifferentException) {
            log.error(Service.exception(key, ex))
            listOf(
                ResultMessage(
                    key,
                    null,
                    reports.fatal(key, ReportMessages.fatalCSV(ex.localizedMessage))
                )
            )
        } catch (ex: InvalidInputException) {
            log.error(Service.exception(key, ex))
            listOf(
                ResultMessage(
                    key,
                    null,
                    reports.fatal(key, ReportMessages.fatalInput(ex.localizedMessage))
                )
            )
        } catch (ex: IllegalArgumentException) { // Sheet index does not exist
            log.error(Service.exception(key, ex))
            listOf(
                ResultMessage(
                    key, null,
                    reports.fatal(key, ReportMessages.fatalSheetIndex(metadata.tableSheetIndex))
                )
            )
        } catch (ex: Exception) {
            log.error(Service.exception(key, ex))
            listOf(
                ResultMessage(
                    key, null,
                    reports.fatal(key, ReportMessages.fatalUnknown(ex::class.java.simpleName, ex.localizedMessage))
                )
            )
        }
    }

    private fun csvMapper(
        key: String,
        value: Message,
        metadata: HeaderMetadata,
        inputStream: InputStream
    ): List<ResultMessage> {
        val resultMessages = mutableListOf<ResultMessage>()
        val identifierSet = mutableSetOf<String>()

        val reader =
            csvReader {
                this.quoteChar = '"'
                this.delimiter = if (value.format == AcceptedFileFormat.CSV) ',' else '\t'
                this.charset = Charsets.UTF_8.displayName()
                // this.skipEmptyLine = true
            }.readAll(inputStream)
        var headerProperties = emptyList<String>()
        var count = 0
        for (line in reader) {
            count += 1
            if (count <= metadata.tableHeaderCount) {
                if (count == metadata.tableHeaderIndex) {
                    headerProperties = line
                    headerProperties = headerProperties.mapIndexed { index, property ->
                        val trimmedProperty = property.trim()
                        if (trimmedProperty.isEmpty()) {
                            throw InvalidInputException(
                                "Missing a property name on row $count in column ${index + 1}!"
                            )
                        }
                        if (trimmedProperty.any { value -> invalidPropertyNameCharacters.contains(value) }) {
                            throw InvalidInputException(
                                "Invalid property name $trimmedProperty on row $count in column ${index + 1}! You may not use the any of the following characters: + , . "
                            )
                        }
                        trimmedProperty
                    }
                }
                continue
            }
            // the -1 ensures, that users can start columns beginning at 1!
            val identifier: String = try {
                line[metadata.tableIdentifierIndex - 1].let { identifierValue ->
                    when (identifierValue) {
                        "" -> {
                            throw InvalidInputException(
                                "The row $count has an empty identifier in column ${metadata.tableIdentifierIndex}."
                            )
                        }

                        in identifierSet -> {
                            throw InvalidInputException(
                                "The row $count contains a duplicated identifier in column ${metadata.tableIdentifierIndex} with another row."
                            )
                        }

                        else -> {
                            identifierSet.add(identifierValue)
                            identifierValue
                        }
                    }
                }
            } catch (ex: InvalidInputException) {
                log.error(Service.exception(key, ex))
                resultMessages.add(
                    ResultMessage(
                        key,
                        null,
                        reports.fatal(key, ReportMessages.fatalInput(ex.localizedMessage))
                    )
                )
                continue
            }
            val keyValueMap = buildJsonObject {
                for ((index, property) in headerProperties.withIndex()) {
                    if (line[index].isNotEmpty()) {
                        put(property, line[index].trim())
                    }
                }
            }
            val report = reports.success(identifier, ReportMessages.success())
            resultMessages.add(ResultMessage(identifier, keyValueMap, report))
        }
        return resultMessages
    }

    private fun excelMapper(
        key: String,
        metadata: HeaderMetadata,
        inputStream: InputStream
    ): List<ResultMessage> {
        val identifierSet = mutableSetOf<String>()
        val propertiesList = mutableListOf<String>()
        inputStream.use { inputStream1 ->
            WorkbookFactory.create(inputStream1).use { workbook ->
                // sheet index is 0-based. This ensures that users can access sheet 1 with index 1!
                val sheet = workbook.getSheetAt(metadata.tableSheetIndex - 1)
                var count = 0
                return sheet.filterEmptyRows().map { row ->
                    count += 1
                    if (count <= metadata.tableHeaderCount) {
                        if (count == metadata.tableHeaderIndex) {
                            propertiesList.addAll(row.map { cell ->
                                if (retrieveCellValue(cell).isNotEmpty()) {
                                    if (retrieveCellValue(cell).any { char ->
                                            invalidPropertyNameCharacters.contains(
                                                char
                                            )
                                        }) {
                                        throw InvalidInputException(
                                            "The property in cell ${cell.address} contains one or more invalid characters: $invalidPropertyNameCharacters."
                                        )
                                    } else {
                                        retrieveCellValue(cell)
                                    }
                                } else {
                                    throw InvalidInputException(
                                        "The header index is missing a value in cell ${cell.address}"
                                    )
                                }
                            }.map { it.trim() })
                        }
                        null
                    } else {
                        try {
                            val rowIdentifier: String = row.getCell(metadata.tableIdentifierIndex - 1).let { cell ->
                                if (cell != null) {
                                    when (val cellValue = retrieveCellValue(cell)) {
                                        "" -> {
                                            throw InvalidInputException(
                                                "The row ${row.rowNum} has an empty identifier in column ${metadata.tableIdentifierIndex}."
                                            )
                                        }

                                        in identifierSet -> {
                                            throw InvalidInputException(
                                                "The row ${row.rowNum} contains a duplicated identifier in column ${metadata.tableIdentifierIndex} with another row."
                                            )
                                        }

                                        else -> {
                                            identifierSet.add(cellValue)
                                            cellValue
                                        }
                                    }
                                } else {
                                    throw InvalidInputException(
                                        "No cell found in row ${row.rowNum} for column ${metadata.tableIdentifierIndex}."
                                    )
                                }
                            }
                            val cells = retrieveCells(row, propertiesList.size - 1)
                            val jsonObject = buildJsonObject {
                                for ((index, property) in propertiesList.withIndex()) {
                                    if (cells[index].isNotEmpty()) {
                                        put(property, cells[index].trim())
                                    }
                                }
                            }
                            ResultMessage(
                                rowIdentifier, jsonObject, reports.success(
                                    rowIdentifier,
                                    ReportMessages.success()
                                )
                            )
                        } catch (ex: InvalidInputException) {
                            log.error(Service.exception(key, ex))
                            ResultMessage(
                                key, null, reports.fatal(
                                    key,
                                    ReportMessages.fatalInput(ex.localizedMessage)
                                )
                            )
                        }
                    }
                    // Empty rows create a null result. These are removed.
                }.filterNotNull()
            }
        }
    }
}
