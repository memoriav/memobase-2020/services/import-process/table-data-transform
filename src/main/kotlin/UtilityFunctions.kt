/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row

object UtilityFunctions {

    /**
     * Retrieves cells from a row of excel. Restricts the size to the actually
     * used part of sheet as otherwise the row is many time larger with many empty cells.
     *
     * @param row: The row from which to retrieve cells.
     * @param size: The number of cells active in the sheet.
     *
     * @return A list of the cell values as strings.
     */
    fun retrieveCells(row: Row, size: Int): List<String> {
        return (0..size).map { i ->
            val cell = row.getCell(i)
            retrieveCellValue(cell)
        }
    }

    /**
     * Checks a cell and returns the content as string. If no valid value is found an empty
     * string is returned.
     *
     * In case of a numeric value, the number is either interpreted as a time (if the number is below one) or
     * as an integer.
     *
     * @param cell: A potential cell.
     * @return Content of the cell as a string.
     */
    fun retrieveCellValue(cell: Cell?): String {
        return if (cell != null) {
            when (cell.cellType) {
                CellType.BLANK -> ""
                CellType.BOOLEAN -> cell.booleanCellValue.toString()
                CellType._NONE -> ""
                CellType.NUMERIC ->
                    if (cell.numericCellValue >= 1) {
                        cell.numericCellValue.toLong().toString()
                    } else {
                        cell.localDateTimeCellValue.toLocalTime().toString()
                    }
                CellType.STRING -> cell.stringCellValue
                CellType.FORMULA -> cell.stringCellValue
                CellType.ERROR -> ""
                else -> ""
            }
        } else ""
    }
}
