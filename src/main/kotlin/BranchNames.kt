package ch.memobase

object BranchNames {
    const val PARSE_SPLIT = "parse-split-"
    const val PARSE_SPLIT_FATAL = "fatal"
    const val PARSE_SPLIT_SUCCESS = "success"

    const val READ_FILE_SPLIT = "read-file-split-"
    const val READ_FILE_SPLIT_FATAL = "fatal"
    const val READ_FILE_SPLIT_SUCCESS = "success"
}