/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.PropertyName.APP_VERSION
import ch.memobase.PropertyName.REPORTING_STEP_NAME
import ch.memobase.settings.HeaderMetadata
import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.message.ObjectMessage

class Service(file: String = "app.yml") {
    companion object {
        fun infoMessage(key: String, message: String, metadata: HeaderMetadata): ObjectMessage {
            return ObjectMessage(
                mapOf(
                    Pair("key", key),
                    Pair("message", message),
                    Pair("recordSetId", metadata.recordSetId),
                    Pair("sessionId", metadata.sessionId)
                )
            )
        }

        fun fatalReport(key: String, message: String, data: Any): ObjectMessage {
            return ObjectMessage(
                mapOf(
                    Pair("key", key),
                    Pair("message", message),
                    Pair("data", data)
                )
            )
        }

        fun exception(key: String, exception: Exception): ObjectMessage {
            return ObjectMessage(
                mapOf(
                    Pair("key", key),
                    Pair("message", exception.localizedMessage),
                    Pair("stackTrace", exception.stackTrace.joinToString("\n"))
                )
            )
        }
    }

    val settings = SettingsLoader(
        listOf(REPORTING_STEP_NAME, APP_VERSION),
        file,
        useStreamsConfig = true,
        readSftpSettings = true
    )

    val topology = KafkaTopology(settings).build()
    private val stream = KafkaStreams(topology, settings.kafkaStreamsSettings)

    fun run() {
        stream.use {
            it.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }
            throw Exception("Stream stopped running!")
        }
    }
}
