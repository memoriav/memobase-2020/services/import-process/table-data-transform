/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.BranchNames.PARSE_SPLIT
import ch.memobase.BranchNames.PARSE_SPLIT_FATAL
import ch.memobase.BranchNames.PARSE_SPLIT_SUCCESS
import ch.memobase.BranchNames.READ_FILE_SPLIT
import ch.memobase.BranchNames.READ_FILE_SPLIT_FATAL
import ch.memobase.BranchNames.READ_FILE_SPLIT_SUCCESS
import ch.memobase.PropertyName.APP_VERSION
import ch.memobase.PropertyName.REPORTING_STEP_NAME
import ch.memobase.exceptions.SftpClientException
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.schema.Message
import ch.memobase.schema.OpenFileOutput
import ch.memobase.schema.ReportMessages
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.SettingsLoader
import ch.memobase.utility.AcceptedFileFormat
import kotlinx.serialization.SerializationException
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Branched
import org.apache.kafka.streams.kstream.Named
import org.apache.logging.log4j.LogManager

class KafkaTopology(private val settings: SettingsLoader) {
    private val log = LogManager.getLogger(this::class.java.name)
    private val step = settings.appSettings.getProperty(REPORTING_STEP_NAME)
    private val stepVersion = settings.appSettings.getProperty(APP_VERSION)
    private val reports = Reports(step, stepVersion)
    private val reader = SftpReader(settings.sftpSettings)
    private val parser = TableParser(reports)
    private val reportingTopic = settings.processReportTopic
    private val acceptedFormats =
        listOf(AcceptedFileFormat.CSV, AcceptedFileFormat.XLSX, AcceptedFileFormat.TSV, AcceptedFileFormat.XLS)

    fun build(): Topology {
        val builder = StreamsBuilder()

        val parseMessageStream = builder
            .stream<String, String>(settings.inputTopic)
            .mapValues { readOnlyKey, value -> parseMessage(readOnlyKey, value) }
            .split(Named.`as`(PARSE_SPLIT))
            .branch(
                { _, value -> value.second.status == ReportStatus.fatal },
                Branched.`as`(PARSE_SPLIT_FATAL)
            )
            .defaultBranch(Branched.`as`(PARSE_SPLIT_SUCCESS))

        parseMessageStream[PARSE_SPLIT + PARSE_SPLIT_FATAL]
            ?.mapValues { value -> value.second.toJson() }
            ?.to(reportingTopic)

        val openFile = parseMessageStream[PARSE_SPLIT + PARSE_SPLIT_SUCCESS]
            ?.filter { _, value -> acceptedFormats.contains(value.first.format) }
            ?.mapValues { value -> value.first }
            ?.processValues(HeaderExtractionSupplier<Message>())
            ?.mapValues { readOnlyKey, value ->
                var message = ""
                val inputStream = try {
                    reader.open(value.first.path)
                } catch (ex: SftpClientException) {
                    message = ex.localizedMessage
                    log.error(Service.exception(readOnlyKey, ex))
                    null
                }
                OpenFileOutput(value.first, value.second, message, inputStream)
            }
            ?.split(Named.`as`(READ_FILE_SPLIT))
            ?.branch(
                { _, value -> value.sftpFile == null },
                Branched.`as`(READ_FILE_SPLIT_FATAL)
            )
            ?.defaultBranch(Branched.`as`(READ_FILE_SPLIT_SUCCESS))

        openFile?.get(READ_FILE_SPLIT + READ_FILE_SPLIT_FATAL)
            ?.mapValues { readOnlyKey, value ->
                log.error("Could not read file at ${value.message.path} because of ${value.exceptionMessage}.")
                reports.fatal(
                    readOnlyKey,
                    "Could not read file at ${value.message.path} because of ${value.exceptionMessage}."
                ).toJson()
            }
            ?.to(reportingTopic)


        val parsedTable = openFile?.get(READ_FILE_SPLIT + READ_FILE_SPLIT_SUCCESS)
            ?.flatMapValues { key, value ->
                parser.parseTable(
                    key,
                    value.message,
                    value.headerMetadata,
                    value.sftpFile!!
                )
            }
            ?.map { _, value -> KeyValue(value.key, value) }

        parsedTable
            ?.mapValues { value -> value.report.toJson() }
            ?.to(reportingTopic)

        parsedTable
            ?.filter { _, value -> value.report.status != ReportStatus.fatal }
            ?.mapValues { value ->
                Json.encodeToString(value.value)
            }
            ?.to(settings.outputTopic)

        return builder.build()
    }


    private fun parseMessage(readOnlyKey: String, data: String): Pair<Message, Report> {
        return try {
            val parsedMessage = Json.decodeFromString<Message>(data)
            Pair(
                parsedMessage,
                reports.success(readOnlyKey, ReportMessages.success())
            )
        } catch (ex: SerializationException) {
            log.error(Service.exception(readOnlyKey, ex))
            Pair(
                Message(AcceptedFileFormat.ERROR, ""),
                reports.fatal(readOnlyKey, ReportMessages.fatalJSON(ex.localizedMessage))
            )
        }
    }
}
