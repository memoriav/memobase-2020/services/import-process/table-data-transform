/*
 * Copyright (C) 2020-present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema

object ReportMessages {
    fun success(): String {
        return "Transformed table to map successfully."
    }

    fun fatalJSON(message: String): String {
        return "JSON ERROR: $message."
    }

    fun fatalCSV(message: String): String {
        return "Invalid CSV Input: $message."
    }

    fun fatalInput(message: String): String {
        return "Invalid Input: $message."
    }

    fun fatalSheetIndex(index: Int): String {
        return "Invalid Sheet Index: The loaded workbook does not have a sheet with index $index."
    }

    fun fatalUnknown(exception: String, message: String): String {
        return "Unexpected Exception: $exception with message $message."
    }
}